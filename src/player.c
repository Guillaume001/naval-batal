#include <player.h>
#include <ship.h>
#include <location.h>
#include <time.h>
#include <grid.h>
#include <stdlib.h>

/**
 * List of players.
 */
static player_t players[NBR_PLAYER];

/**
 * Create a player and initialize his ships.
 *
 * @param num_player: identifiant of player
 * @return player_t with his number and unplaced boats
 */
static player_t create_player(int num_player)
{
    player_t player;
    player.number = num_player;
    player.ships_number = LAST_SHIP;

    for (int i = 0; i < player.ships_number; i++)
        ship_init(&player.ships[i], i);
    return player;
}

int one_ship_still_alive(player_t *player_ptr)
{
    for (int i = 0; i < player_ptr->ships_number; i++)
        if (ship_still_alive(&player_ptr->ships[i]))
            return 1;
    return 0;
}

/**
 * Get random a ship alive from a player.
 *
 * @param player_ptr: pointer of player
 * @return structure of ship alive
 */
static ship_t *get_random_alive_ship(player_t *player_ptr)
{
    ship_t *random_ship = NULL;
    do
    {
        int index = rand() % player_ptr->ships_number;
        random_ship = &player_ptr->ships[index];
    } while (!ship_still_alive(random_ship));
    return random_ship;
}

action_t get_action(player_t *player_ptr)
{
    ship_t *ship = get_random_alive_ship(player_ptr);
    int start_point = get_random_start_point(ship);
    location_t start = {ship->loc.x, ship->loc.y + start_point};
    return (action_t){start, get_fire_target(start, player_ptr->number)};
}

int player_can_play(player_t *player_ptr)
{
    int shots = 0;
    for (int i = 0; i < player_ptr->ships_number; i++)
    {
        if (ship_still_alive(&player_ptr->ships[i]))
            for (int j = 0; j < ship_length(&player_ptr->ships[i]); j++)
            {
                location_t start = {player_ptr->ships[i].loc.x,
                                    player_ptr->ships[i].loc.y + j};
                location_t end = get_fire_target(start, player_ptr->number);
                if (is_inside_grid(end))
                    shots++;
            }
    }
    return shots > 0;
}

void initialize_players()
{
    for (int i = 0; i < NBR_PLAYER; i++)
        players[i] = create_player(i);
}

void insert_ships()
{
    for (int i = 0; i < NBR_PLAYER; i++)
        for (int j = 0; j < players[i].ships_number; j++)
        {
            ship_t *ship = &players[i].ships[j];
            location_t location;
            do
            {
                location = get_random_position();
            } while (!ship_insert(&players[i], ship, location));
        }
}

player_t *get_player(int identifiant){
    if(identifiant >= NBR_PLAYER || identifiant < 0)
        return NULL;
    return &players[identifiant];
}
#include <stdlib.h>
#include <grid.h>
#include <ship.h>

/**
  * Main game grid composed of cells.
  */
static cell_t grid[GRID_LENGTH][GRID_LENGTH];

void initialize_grid()
{
	for (int i = 0; i < GRID_LENGTH; i++)
	{
		for (int j = 0; j < GRID_LENGTH; j++)
		{
			cell_t cell = {
				.annihilated = 0,
			};
			grid[i][j] = cell;
		}
	}
}

int is_inside_grid(location_t loc)
{
	return loc.x < GRID_LENGTH && loc.x >= 0 && loc.y < GRID_LENGTH && loc.y >= 0;
}

/**
 * This function test if the coordinates correspond to the grid size,
 * also it verify if the ship length allow to enter in the grid.
 * 
 * @param player_ptr: player who owns the ship
 * @param ship_ptr: structure which correspond to a ship
 * @param destination: structure which correspond to a destination location
 */
static int test_location(player_t *player_ptr, ship_t *ship_ptr, location_t destination)
{
	int length = ship_length(ship_ptr);

	if (!is_inside_grid(destination) || destination.y + length > GRID_LENGTH)
	{
		return 0;
	}

	for (int i = 0; i < length; i++)
		if (get_cell_coord(destination.x, destination.y + i)->ships[player_ptr->number] != NULL)
			return 0;

	return 1;
}

int ship_insert(player_t *player_ptr, ship_t *ship_ptr, location_t destination)
{
	if (is_inside_grid(ship_ptr->loc))
		return 0;

	int ship_len = ship_length(ship_ptr);

	if (test_location(player_ptr, ship_ptr, destination))
	{
		ship_ptr->loc = destination;
		for (int i = 0; i < ship_len; i++)
			get_cell_coord(destination.x, destination.y + i)->ships[player_ptr->number] = ship_ptr;
		return 1;
	}
	return 0;
}

cell_t *get_cell_loc(location_t location)
{
	if (!is_inside_grid(location))
		return NULL;
	return &grid[location.x][location.y];
}

cell_t *get_cell_coord(int coord_x, int coord_y)
{
	location_t loc = {coord_x, coord_y};
	return get_cell_loc(loc);
}

void annihilate_location(location_t location)
{
	if (!is_inside_grid(location))
		return;
	cell_t *cell = get_cell_loc(location);
	cell->annihilated = 1;
	for (int i = 0; i < NBR_PLAYER; i++)
		damaged_ship_at(cell->ships[i], location);
}

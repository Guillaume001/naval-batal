#include <stdlib.h>
#include <ship.h>
#include <grid.h>

/**
 * Indicates the size of each ship corresponding to the enumeration.
 *
 * @see enum ship_kind
 */
static const int length[] = {1, 2, 3, 4, 5};

int ship_length(ship_t *ship_ptr)
{
    return length[ship_ptr->kind];
}

void ship_init(ship_t *ship_ptr, enum ship_kind kind)
{
    ship_ptr->kind = kind;
    ship_ptr->loc = create_location();
    for (int i = 0; i < LAST_SHIP; i++)
        ship_ptr->states[i] = 0;
}

int ship_still_alive(ship_t *ship_ptr)
{
    for (int i = 0; i < ship_length(ship_ptr); i++)
        if (!ship_ptr->states[i])
            return 1;
    return 0;
}

/**
 * Find out if a location is valid in a array.
 * 
 * @param locations: array of location
 * @return 1 only if at least one location is valid in the array else 0
 */
static int at_least_one_location_is_valid(location_t locations[TORPEDO_DIRECTIONS])
{
    for (int i = 0; i < TORPEDO_DIRECTIONS; i++)
        if (is_inside_grid(locations[i]))
            return 1;
    return 0;
}

/**
 * Randomly indicates a location to be torpedoed from a list of possibilities.
 * 
 * @param locations: array of possible locations
 * @return location_t to be torpedoed
 */
static location_t get_random_location_in_targeted(location_t locations[TORPEDO_DIRECTIONS])
{
    if (!at_least_one_location_is_valid(locations))
        return create_location();

    location_t random_location = create_location();
    do
    {
        int index = rand() % TORPEDO_DIRECTIONS;
        random_location = locations[index];
    } while (!is_inside_grid(random_location));
    return random_location;
}

/**
 * Allows you to retrieve a ship's index number from a location on the grid.
 * 
 * @param ship_ptr: ship concerned by the location
 * @param location: grid location
 * @return index in the ship corresponding of the location in parameter
 */
static int get_position_in_ship(ship_t *ship_ptr, location_t location)
{
    return location.y - ship_ptr->loc.y;
}

/**
 * Gets the status of the piece of the ship according to a location.
 * 
 * @param ship_ptr: ship concerned by the location
 * @param location: grid location
 * @return 0 if the status is correct otherwise 1
 */
static int get_state_at(ship_t *ship_ptr, location_t location)
{
    if (ship_ptr == NULL)
        return -1;
    return ship_ptr->states[get_position_in_ship(ship_ptr, location)];
}

void damaged_ship_at(ship_t *ship_ptr, location_t location)
{
    if (ship_ptr == NULL || !is_inside_grid(location))
        return;
    ship_ptr->states[get_position_in_ship(ship_ptr, location)] = 1;
}

/**
 * Calculates a location to be torpedoed from a location in the grid and
 * the number of the shooting player.
 * 
 * @param start_location: torpedo firing start location
 * @param num_player: identifying the player firing the torpedo
 * @return the location the torpedo reached without affecting play.
 *         If the torpedo doesn't hit an opposing target the location is initialized outside grid.
 */
static location_t get_location_to_be_torpedoed(location_t start_location, int num_player)
{
    int vectors[][2] = {{1, 0}, {0, 1}, {-1, 0}, {0, -1}};
    location_t locations_targeted[TORPEDO_DIRECTIONS];
    for (int i = 0; i < TORPEDO_DIRECTIONS; i++)
        locations_targeted[i] = create_location();
    for (int range = 1; range <= MAXI_TORPEDO_RANGE; range++)
    {
        for (int v = 0; v < TORPEDO_DIRECTIONS; v++)
        {
            location_t current_location = {start_location.x + vectors[v][0] * range,
                                           start_location.y + vectors[v][1] * range};
            if (is_inside_grid(current_location))
            {
                cell_t *cell = get_cell_loc(current_location);
                for (int i = 0; i < NBR_PLAYER; i++)
                {
                    int state = get_state_at(cell->ships[i], current_location);
                    if (i != num_player && !cell->annihilated && state == 0)
                        locations_targeted[v] = current_location;
                }
            }
        }
        if (at_least_one_location_is_valid(locations_targeted))
            break;
    }
    return get_random_location_in_targeted(locations_targeted);
}

int get_random_start_point(ship_t *ship_ptr)
{
    int random_start_point = -1;
    do
    {
        random_start_point = rand() % ship_length(ship_ptr);
    } while (ship_ptr->states[random_start_point] != 0);
    return random_start_point;
}

location_t get_fire_target(location_t start_location, int num_player)
{
    if (!is_inside_grid(start_location))
        return create_location();
    if (num_player < 0 || num_player >= NBR_PLAYER)
        return create_location();
    if (get_state_at(get_cell_loc(start_location)->ships[num_player], start_location))
        return create_location();
    return get_location_to_be_torpedoed(start_location, num_player);
}

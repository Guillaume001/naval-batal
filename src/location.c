#include <stdlib.h>
#include <location.h>
#include <grid.h>
#include <unistd.h>
#include <time.h>

/**
 * Number indicating that the location is not defined.
 */
#define MAGIC_NUMBER_UNSET -1

location_t get_random_position()
{
    return (location_t){rand() % GRID_LENGTH, rand() % GRID_LENGTH};
}

location_t create_location()
{
    return (location_t){MAGIC_NUMBER_UNSET, MAGIC_NUMBER_UNSET};
}
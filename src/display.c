#include <display.h>
#include <grid.h>
#include <stdio.h>
#include <unistd.h>
#include <time.h>
#include <string.h>

/** @brief Definition of constant which correspond to the ASCII code of a letter A
 * This constant is used in order to display the horizontal letters related of each column
 */
#define ASCII_CODE_A 65

/**
 * Color code to reset the terminal color.
 */
#define RESET_COLOR "\e[0m"

/**
 * Color code for red color.
 */
#define RED "\e[0;31m"

/**
 * Color code for green color.
 */
#define GREEN "\e[0;32m"

/**
 * Color code for blue color.
 */
#define BLUE "\e[0;34m"

/**
 * Function to clear terminal and keep history.
 */
#define clear() printf("\033[H\033[2J")

/**
 * Function to move cursor into terminal in specific coordonates.
 *
 * @param x: x-axis coordinates
 * @param y: y-axis coordinates
 */
#define move_cursor_to_xy(x, y) printf("\033[%d;%dH", (x), (y))

/**
 * Color array for players.
 */
static const char *colors_by_player[NBR_PLAYER] = {GREEN, BLUE};

/**
 * Realizes a break line thanks to the parametric coordinate.
 * 
 * @param location: pointer of location corresponding to the current line
 */
static void break_line(location_t *location)
{
    move_cursor_to_xy(location->x, location->y);
    location->x++;
    fflush(stdout);
}

/**
 * This function display the horizontal letters related of each column.
 * 
 * @param location: pointer of location where print the first line
 */
static void display_first_line(location_t *location)
{
    printf("║   ║ ");
    for (int i = 0; i < GRID_LENGTH; i++)
    {
        printf("%c ║ ", ASCII_CODE_A + i);
    }
    break_line(location);
}

/**
 * This function display the separation of each row.
 * 
 * @param line_type: 0 to print first line, 1 for middle separator line and 2 for end line.
 * @param location: pointer of location where print the separator line
 */
static void display_separator_line(int line_type, location_t *location)
{
    if (line_type == 0)
        printf("╔");
    else if (line_type == 2)
        printf("╚");
    else
        printf("╠");

    printf("═══");
    char *separator = "╬";
    if (line_type == 0)
        separator = "╦";
    else if (line_type == 2)
        separator = "╩";
    printf("%s", separator);

    for (int i = 0; i < GRID_LENGTH - 1; i++)
        printf("═══%s", separator);
    printf("═══");

    if (line_type == 0)
        printf("╗");
    else if (line_type == 2)
        printf("╝");
    else
        printf("╣");
    
    break_line(location);
}

/**
 * This function display the cell on NBR_PLAYER+1 characters.
 *
 * @param cell: cell you want to display
 * @param row_number: number of the current row
 */
static void print_cell(cell_t cell, int row_number)
{
    for (int i = 0; i < NBR_PLAYER; i++)
    {
        printf("%s", colors_by_player[i]);
        if (cell.ships[i] == NULL)
            printf(" ");
        else
        {
            if (cell.ships[i]->states[row_number - cell.ships[i]->loc.y] == 1)
                printf("░");
            else
                printf("█");
        }
        printf("%s", RESET_COLOR);
    }

    if (cell.annihilated)
        printf("%s╳%s", RED, RESET_COLOR);
    else
        printf(" ");
}

/**
 * This function display the vertical numbers related of each row
 * And display the separators of each column in order to obtain squares.
 *
 * @param numb_row: numbers related of each row
 * @param location: pointer of location where print the separator line
 */
static void display_line(int numb_row, location_t *location)
{
    printf("║%2d ║", numb_row + 1);
    for (int i = 0; i < GRID_LENGTH; i++)
    {
        print_cell(*get_cell_coord(numb_row, i), i);
        printf("║");
    }
    break_line(location);
}

/**
 * Displays the game grid from the top left coordinate.
 *
 * @param location: top left location
 */
static void display_grid(location_t location)
{
    break_line(&location);
    display_separator_line(0, &location);
    display_first_line(&location);
    for (int i = 0; i < GRID_LENGTH; i++)
    {
        display_separator_line(1, &location);
        display_line(i, &location);
    }
    display_separator_line(2, &location);
}

/**
 * Displays the lines corresponding to the actions played.
 *
 * @param actions: array of player's action
 */
static void display_actions(action_t actions[])
{
    for (int i = 0; i < NBR_PLAYER; i++)
    {
        location_t start_point = actions[i].start_point;
        location_t target_point = actions[i].target_point;
        printf("%sPlayer%s shoots from %c%d", colors_by_player[i], RESET_COLOR,
               ASCII_CODE_A + start_point.y, start_point.x + 1);
        if (is_inside_grid(target_point))
            printf(" to %c%d\n", ASCII_CODE_A + target_point.y, target_point.x + 1);
        else
            printf(" in water\n");
    }
}

/**
 * Displays a character in a grid cell and at the player's location.
 *
 * @param grid_origin: top left location of grid
 * @param location: location of the cell where print
 * @param num_player: identifier of player
 * @param character: character to print
 */
static void print_character_in_grid(location_t grid_origin, location_t location,
                                    int num_player, char *character)
{
    location_t print_loc = {grid_origin.x + 3, grid_origin.y + 6 + num_player};
    location_t movement = {2, 4};
    move_cursor_to_xy(print_loc.x + movement.x * location.x, print_loc.y + movement.y * location.y);
    if (num_player < NBR_PLAYER)
        printf("%s", colors_by_player[num_player]);
    printf("%s", character);
    if (num_player < NBR_PLAYER)
        printf("%s", RESET_COLOR);
    fflush(stdout);
}

/**
 * Indicates the direction of the torpedo's fire according to the player's action.
 *
 * @param action: player's action
 * @return torpedo direction index
 */
static int torpedo_way(action_t action)
{
    if (action.start_point.x != action.target_point.x)
    {
        if (action.start_point.x < action.target_point.x)
            return 0;
        return 1;
    }
    if (action.start_point.y != action.target_point.y)
    {
        if (action.start_point.y < action.target_point.y)
            return 2;
        return 3;
    }
    return -1;
}

/**
 * Displays the animation of a player's action.
 *
 * @param action: player's action
 * @param grid_origin: top left location of grid
 * @param player: identifier of the player who is playing
 */
static void display_toperdo_fire(action_t action, location_t grid_origin, int player)
{
    if (is_inside_grid(action.start_point) != 1)
        return;
    location_t movement = action.start_point;
    int way = torpedo_way(action);
    if (way == -1)
        return;
    const int direction[][2] = {{1, 0}, {-1, 0}, {0, 1}, {0, -1}};
    char *arrow_direction[] = {"▼", "▲", "►", "◄"};
    if (!(action.target_point.x != -1 && action.target_point.y != 1))
        return;
    print_character_in_grid(grid_origin, movement, player, "O");
    while ((movement.x != action.target_point.x && movement.y != action.target_point.y - 1) 
        || (movement.x != action.target_point.x - 1 && movement.y != action.target_point.y))
    {
        movement.x += direction[way][0];
        movement.y += direction[way][1];
        print_character_in_grid(grid_origin, movement, player, arrow_direction[way]);
    };
    print_character_in_grid(grid_origin, movement, player, "╳");
    print_character_in_grid(grid_origin, movement, NBR_PLAYER, " ");
}

/**
 * Displays the arrow between the two grids.
 *
 * @param info: message to print
 * @param offset_x: padding top
 * @param grid_width: grid width in characters
 * @param grid_height: grid height in characters
 */
static int display_arrow_between(char *info, int offset_x, int grid_width, int grid_height)
{
    int offset = strlen(info);
    move_cursor_to_xy(offset_x + grid_height / 2 - 1, grid_width + 2);
    printf("%s", info);
    move_cursor_to_xy(offset_x + grid_height / 2, grid_width + 2);
    for (int i = 0; i < offset - 1; i++)
        printf("-");
    printf("►");

    return offset + 4;
}

void display_game(int round, action_t actions[])
{
    clear();
    printf("Round %d :\n", round);
    display_actions(actions);

    int offset_x = NBR_PLAYER + 3;
    int grid_width = GRID_LENGTH * (NBR_PLAYER + 1) + GRID_LENGTH + 6;
    int grid_height = GRID_LENGTH * 2 + 2;
    display_grid((location_t){offset_x, 0});

    for (int i = 0; i < NBR_PLAYER; i++)
        display_toperdo_fire(actions[i], (location_t){offset_x, 0}, i);

    int dist_between = display_arrow_between("After shootings", offset_x, grid_width, grid_height);

    display_grid((location_t){offset_x, grid_width + dist_between});

    printf("\n"); // Put correctly the cursor on the new line
    sleep(1);
}
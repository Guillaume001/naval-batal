#include <stdlib.h>
#include <grid.h>
#include <display.h>
#include <ship.h>
#include <player.h>
#include <unistd.h>
#include <time.h>

/**
 * Main loop of the game.
 */
void main_loop()
{
    int round = 1;
    int players_can_play = 1;
    while (players_can_play)
    {
        action_t actions[NBR_PLAYER];
        for (int i = 0; i < NBR_PLAYER; i++)
        {
            if (!player_can_play(get_player(i)))
            {
                players_can_play = 0;
                break;
            }
            actions[i] = get_action(get_player(i));
        }
        if (!players_can_play)
            break;
        for (int i = 0; i < NBR_PLAYER; i++)
            annihilate_location(actions[i].target_point);
        display_game(round, actions);
        round++;
    }
}

/**
 * Main function of the program.
 * 
 * @param arcv: number of arguments
 * @param argv: list of arguments
 */
int main(int arcv, char *argv[])
{
    (void)arcv;
    (void)argv;

    srand(time(NULL)); // Initialization of random counter

    initialize_grid();
    initialize_players();
    insert_ships();
    main_loop();

    return EXIT_SUCCESS;
}

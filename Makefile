CC=gcc
H_PATH=include
SRC_PATH=src
TEST_PATH=test
OBJ_PATH=bin
CFLAGS=-std=c99 -Wall -Werror
LDFLAGS=-I$(H_PATH)
GCC_OPTS=
EXEC=project

SRC=$(wildcard $(SRC_PATH)/*.c)
OBJ=$(subst $(SRC_PATH),$(OBJ_PATH),$(SRC:.c=.o))

all: $(EXEC)

########### PROGRAM ###########
$(EXEC): $(OBJ) 
	$(CC) -o $@ $^ $(CFLAGS) $(LDFLAGS) $(GCC_OPTS)

$(OBJ_PATH)/%.o: $(SRC_PATH)/%.c install
	$(CC) -o $@ -c $< $(LDFLAGS) $(CFLAGS) $(GCC_OPTS)

########### UNIT TEST ###########
test:
	@make clean
	@cd $(TEST_PATH) && make

########### COVERAGE ###########
coverage:
	@make clean
	@cd $(TEST_PATH) && make GCC_OPTS='-O0 --coverage'
	gcovr -r . -e $(TEST_PATH) -e $(SRC_PATH)/main.c -e $(SRC_PATH)/display.c
	@mkdir -p coverage_html
	@gcovr -r . -e $(TEST_PATH) -e $(SRC_PATH)/main.c -e $(SRC_PATH)/display.c --html --html-details -o coverage_html/index.html

########### LINT ###########
lint: $(SRC)
	oclint -report-type html -o oclint_result.html $^ -- $(CFLAGS) $(LDFLAGS)

########### VALGRIND ###########
valgrind: $(EXEC)
	valgrind --tool=memcheck --leak-check=yes --show-reachable=yes --num-callers=20 --error-exitcode=1 ./$^

########### DOXYGEN DOCUMENTATION ###########
doc:
	@cd doc && make
	@cd doc/latex && make

########### ENGLISH REPORT ###########
report:
	@cd report && make

########### CLEAN ###########
clean:
	@rm -rf *$(EXEC) $(OBJ_PATH)/* oclint_result.html
	@rm -rf *.gcda *.gcno *_test
	@rm -rf coverage_html/
	@cd doc && make clean
	@cd test && make clean
	@cd report && make clean

install:
	@mkdir -p $(OBJ_PATH)

.PHONY: doc report lint test

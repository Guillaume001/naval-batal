/** @file location.h
 *
 * Library for manipulating locations.
 *
 */
#ifndef LOCATION_H
#define LOCATION_H

/**
 * @brief Definition of structure which correspond to a location.
 *
 * @param x: location on the row (horizontally)
 * @param y: location on the column (vertically)
 */
typedef struct
{
    int x;
    int y;
} location_t;

/**
 * Generates random coordinates in the game grid.
 * 
 * @return location_t which contains a random x and y contained in the game grid
 */
location_t get_random_position();

/**
 * Create a location structure not initialize.
 * 
 * @return strcture location with -1 in x and y
 */
location_t create_location();

#endif
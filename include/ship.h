/** @file ship.h
 * 
 * This library provide a few functions to insert a ship on the game grid.
 *
 */
#ifndef SHIP_H
#define SHIP_H

#include <location.h>

/**
 * @brief Definition of a constant which correspond to the maximal range for a seeker torpedo.
 */
#define MAXI_TORPEDO_RANGE GRID_LENGTH / 2

/**
 * @brief Definition of a constant which correspond to the all possible directions of the torpedo.
 */
#define TORPEDO_DIRECTIONS 4

/**
 * @brief Definition of enumeration which correspond to the length of the ship.
 *
 * The name of a each ship correspond to a length
 */
enum ship_kind
{
    PORTE_ETENDAL,
    SUBAQUAL,
    CONTRE_TORPAL,
    CROISAL,
    PORTAVIAL,
    LAST_SHIP
};

/**
 * @brief Definition of structure which correspond to a ship.
 *
 * @param kind: name and length of a ship
 * @param loc: location on the grid
 * @param states: array of the status of the ship's parts, 0 indicates the good status and 1 indicates the destroyed status.
 *
 * These coordinates correspond to the first cell on the top left of a ship.
 */
typedef struct
{
    enum ship_kind kind;
    location_t loc;
    int states[LAST_SHIP];
} ship_t;

/**
 * This function return the length of a ship by means of his name whish is an enumeration.
 *
 * @param ship_ptr: structure which correspond to a ship
 * @return size of the ship
 */
int ship_length(ship_t *ship_ptr);

/**
 * Initialize the ship with his kind.
 *
 * @param ship_ptr: pointer of ship to initialize
 * @param kind: kind of ship
 */
void ship_init(ship_t *ship_ptr, enum ship_kind kind);

/**
 * This function allow to know if a ship is still alive.
 * 
 * @param ship_ptr: pointer of ship
 * @return 1 only if one part or more of the ship is still alive, 0 else
 */
int ship_still_alive(ship_t *ship_ptr);

/**
 * This function allows to a ship to get target thanks to a seeker torpedo to hit the nearest enemy ship.
 * 
 * @param start_location: starting location of the shot
 * @param num_player: identifiant of shooting player
 * @return location that corresponds to the location that the torpedo can obliterate or (-1,-1) if the torpedo falls into the water
 */
location_t get_fire_target(location_t start_location, int num_player);

/**
 * Retrieves the index to a live location of a ship.
 *
 * @param ship_ptr: pointer of alive ship
 * @return index of alive location
 */
int get_random_start_point(ship_t *ship_ptr);

/**
 * Function that allows you to hit a piece of a boat.
 * 
 * @param ship_ptr: pointer of ship
 * @param location: location inside the ship to annihilate
 */
void damaged_ship_at(ship_t *ship_ptr, location_t location);

#endif

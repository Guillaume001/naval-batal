/** @file display.h
 * 
 * This library provides a few functions to display the grid of a naval batal game.
 *
 */
#ifndef DISPLAY_H
#define DISPLAY_H

#include <player.h>

/**
 * Displays the status during a game turn with all information.
 * 
 * @param round: round number
 * @param actions: list of actions the players have played
 */
void display_game(int round, action_t actions[]);

#endif
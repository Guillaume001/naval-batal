/** @file grid.h
 *
 * This library provide a few functions linked by the players.
 *
 */
#ifndef PLAYER_H
#define PLAYER_H

#include <ship.h>
#include <location.h>

/**
 * Number of player.
 */
#define NBR_PLAYER 2

/**
 * @brief Structure for representing a player.
 *
 * @param number: identifiant
 * @param ships_number: number of ships owned by the player
 * @param ships: list of ships owned by the player
 */
typedef struct
{
    int number;
    int ships_number;
    ship_t ships[LAST_SHIP];
} player_t;

/**
 * @brief Strcutre representing a player's action.
 *
 * @param start_point: starting location of the shot
 * @param target_point: ending location of the shot, (-1,-1) significate in water
 */
typedef struct
{
    location_t start_point;
    location_t target_point;
} action_t;

/**
 * Indicates if a player's ship is still alive.
 * 
 * @param player_ptr: pointer of player
 * @return 1 if and only if at least one ship of the player is alive otherwise 0
 */
int one_ship_still_alive(player_t *player_ptr);

/**
 * Ask the player what action he wants to play. This function has no effect on the game.
 *
 * @param player_ptr: player who wants to play
 */
action_t get_action(player_t *player_ptr);

/**
 * Indicate if the player can play.
 * 
 * @param player_ptr: pointer of player
 */
int player_can_play(player_t *player_ptr);

/**
 * Initialize each player with unplaced ships.
 */
void initialize_players();

/**
 * Insert all unplaced ships in grid.
 */
void insert_ships();

/**
 * Allows to recover a player thanks to his identifiant.
 * 
 * @param identifiant: player identifier
 * @return pointer of player or null if identifiant is not valid
 */
player_t *get_player(int identifiant);

#endif

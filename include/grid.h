/** @file grid.h
 * 
 * This library provides some functions to manage the grid.
 *
 */
#ifndef GRID_H
#define GRID_H

#include <location.h>
#include <ship.h>
#include <player.h>

/**
 * Definition of constant which correspond to the size of the game grid.
 * 
 * The size of the 2D board is GRID_LENGTH * GRID_LENGTH
 */
#define GRID_LENGTH 10

/**
 * @brief Definition of structure which correspond to a cell.
 * 
 * @param ships: list of pointer of ship, the position is linked with the player
 */
typedef struct
{
    int annihilated;
    ship_t *ships[NBR_PLAYER];
} cell_t;

/**
 * This function display the whole grid of a naval ship game.
 */
void initialize_grid();

/**
 * This function tests if the position is inside the grid.
 * 
 * @param loc: location (x, y)
 * @return 1 only if this position is inside the grid else 0
 */
int is_inside_grid(location_t loc);

/**
 * This function insert a ship in the game grid if the location test succeed.
 * 
 * @param player_ptr: player who owns the ship
 * @param ship_ptr: ship you want to place
 * @param destination: destination location you want for this ship
 * @return 1 only if the insertion of a ship is succeed, 0 else
 */
int ship_insert(player_t *player_ptr, ship_t *ship_ptr, location_t destination);

/**
 * This function allow to annihilate a cell or to know if a cell is already annihilate.
 *
 * @param location: location to be annihilated
 */
void annihilate_location(location_t location);

/**
 * Allows to recover a cell thanks to a location.
 * 
 * @return cell at location in grid
 */
cell_t *get_cell_loc(location_t location);

/**
 * Allows to recover a cell thanks to a coordinates (x, y).
 * 
 * @param coord_x: x-axis coordinates
 * @param coord_y: y-axis coordinates
 * @return cell at coordinates (x,y) in grid
 */
cell_t *get_cell_coord(int coord_x, int coord_y);

#endif
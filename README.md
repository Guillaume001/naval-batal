<img src="report/images/ship.png" width="120" alt="Naval Batal Logo" />

# Naval Batal

## Description

This Naval Batal programme is carried out as part of a project during the 2nd semester of Networks and Information Systems training at ENSEIRB-MATMECA.
Pairs were created to work on the same [french subject](doc/french_subject.pdf) in parallel.
The goal of this project is to develop a game inspired by the traditional naval battle but with special rules.

We answered the topic until achievement 1. It is possible to visualize the project at the end of each step thanks to the tags.

To view the project in the baseline state:

```
git checkout tags/baseline 
```

To view the project at the end of achievement 1:

```
git checkout tags/achievement_1 
```

## Usage

To use the program to the bare minimum, you need the following tools: git, gcc and make.

1. Clone git repository
```
git clone https://thor.enseirb-matmeca.fr/git/pg111-8965
```
2. Change directory
```
cd pg111-8965
```
3. Compile the program
```
make
```
4. Run program
```
./project
```

## Documentation

**Detailled functional specifications**

You can build a detailed functional speficiations file in PDF format in the report folder with this command:

```
make report
```

WARNING: You must be able to use all packages included in the structure file.  

**Code documentation**

You can generate the [Doxygen](http://www.doxygen.nl/) documentation in doc folder with this command:

```
make doc
```

## Developer usage

The make file allows you to check a set of parameters quickly.

**Unit test**

To run the unit tests you can use the following command:

```
make test
```

**Test coverage**

You must have [gcovr](https://gcovr.com/en/stable/guide.html) installed before.
To get test coverage you can use the following command:

```
make coverage
```

**Code analysis**

You must have [oclint](http://oclint.org/) installed before.
To analysis of code you can use the following command:

```
make lint
```

**Check memory leaks**

You must have [valgrind](https://valgrind.org/) installed before.
To check memory leaks you can use the following command:

```
make valgrind
```

## Developer team

Our binomial is as follows:
* COURS Guillaume
* LUCAS Léandre

## License

The Naval Batal project is open-source software licensed under the [CeCILL](LICENSE).
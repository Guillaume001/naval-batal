#ifndef SHIP_TEST_H
#define SHIP_TEST_H

#include <ship.h>

void ship_test();
void test_s_init(ship_t ship, enum ship_kind kind);
void sunk_ship(ship_t *ship);

#endif
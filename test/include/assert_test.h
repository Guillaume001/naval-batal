#ifndef ASSERT_TEST_H
#define ASSERT_TEST_H

#include <stdlib.h>
#include <stdio.h>
#include <assert.h>

#define assert_equal(expected, actual)                                                     \
    if ((expected) != (actual))                                                            \
    {                                                                                      \
        printf("\033[1;31mError : expected %d but was %d\033[0m\n", (expected), (actual)); \
        assert((expected) == (actual));                                                    \
    }

#define assert_no_equal(expected, actual)                                                  \
    if ((expected) == (actual))                                                            \
    {                                                                                      \
        printf("\033[1;31mError : expected %d but was %d\033[0m\n", (expected), (actual)); \
        assert((expected) != (actual));                                                    \
    }

#endif
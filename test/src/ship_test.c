#include <ship.h>
#include <grid.h>
#include <location.h>
#include <assert_test.h>

static void test_ship_length()
{
    ship_t ship = {.kind = PORTE_ETENDAL};
    assert_equal(1, ship_length(&ship));
    ship.kind = SUBAQUAL;
    assert_equal(2, ship_length(&ship));
    ship.kind = CONTRE_TORPAL;
    assert_equal(3, ship_length(&ship));
    ship.kind = CROISAL;
    assert_equal(4, ship_length(&ship));
    ship.kind = PORTAVIAL;
    assert_equal(5, ship_length(&ship));
}

void test_s_init(ship_t ship, enum ship_kind kind)
{
    assert_equal(ship.kind, kind);
    assert_equal(ship.loc.x, -1);
    assert_equal(ship.loc.y, -1);
    for (int i = 0; i < LAST_SHIP; i++)
        assert_equal(0, ship.states[i]);
}

static void test_ship_init()
{
    ship_t ship;
    ship_init(&ship, CROISAL);
    test_s_init(ship, CROISAL);

    ship.states[0] = 452;
    ship.states[3] = 4;
    ship.states[LAST_SHIP] = 5;

    ship_init(&ship, PORTAVIAL);
    test_s_init(ship, PORTAVIAL);
}

static void test_ship_still_alive()
{
    ship_t ship;
    ship_init(&ship, CROISAL);
    assert_equal(1, ship_still_alive(&ship));

    for (int i = ship_length(&ship); i < LAST_SHIP; i++)
        ship.states[i] = 1;
    assert_equal(1, ship_still_alive(&ship));

    for (int i = 1; i < ship_length(&ship); i++)
        ship.states[i] = 1;
    assert_equal(1, ship_still_alive(&ship));

    ship.states[0] = 1;
    assert_equal(0, ship_still_alive(&ship));
}

void sunk_ship(ship_t *ship)
{
    for (int i = 0; i < ship_length(ship); i++)
        ship->states[i] = 1;
}

static void test_torpedo(int coord_x, int coord_y, int num_player, int excepted_x, int excepted_y)
{
    location_t start = {coord_x, coord_y};
    location_t end = get_fire_target(start, num_player);
    assert_equal(excepted_x, end.x);
    assert_equal(excepted_y, end.y);
}

static void test_get_fire_target()
{
    initialize_grid();
    initialize_players();
    player_t player1 = *get_player(0);
    ship_t ship11 = player1.ships[CROISAL];
    location_t loc11 = {.x = 0, .y = 0};
    assert_equal(1, ship_insert(&player1, &ship11, loc11));

    player_t player2 = *get_player(1);
    ship_t ship21 = player2.ships[PORTAVIAL];
    location_t loc21 = {.x = 6, .y = 0};
    assert_equal(1, ship_insert(&player2, &ship21, loc21));

    test_torpedo(-1, -1, player1.number, -1, -1);
    test_torpedo(0, 0, NBR_PLAYER + 5, -1, -1);
    test_torpedo(0, 0, -2, -1, -1);
    test_torpedo(0, 0, player1.number, -1, -1);
    test_torpedo(0, 1, player1.number, -1, -1);
    test_torpedo(0, 2, player1.number, -1, -1);

    ship_t ship22 = player2.ships[SUBAQUAL];
    location_t loc22 = {.x = 0, .y = 8};
    assert_equal(1, ship_insert(&player2, &ship22, loc22));
    test_torpedo(0, 3, player1.number, 0, 8);
    ship22.states[0] = 1;
    test_torpedo(0, 8, player2.number, -1, -1);

    ship_t ship12 = player1.ships[CONTRE_TORPAL];
    location_t loc12 = {.x = 8, .y = 3};
    assert_equal(1, ship_insert(&player1, &ship12, loc12));

    ship_t ship13 = player1.ships[PORTE_ETENDAL];
    location_t loc13 = {.x = 8, .y = 8};
    assert_equal(1, ship_insert(&player1, &ship13, loc13));

    ship_t ship14 = player1.ships[CROISAL];
    location_t loc14 = {.x = 6, .y = 6};
    assert_equal(1, ship_insert(&player1, &ship14, loc14));

    ship_t ship23 = player2.ships[CONTRE_TORPAL];
    location_t loc23 = {.x = 4, .y = 4};
    assert_equal(1, ship_insert(&player2, &ship23, loc23));

    ship_t ship24 = player2.ships[PORTE_ETENDAL];
    location_t loc24 = {.x = 8, .y = 9};
    assert_equal(1, ship_insert(&player2, &ship24, loc24));

    test_torpedo(4, 4, player2.number, 8, 4);
    location_t end = get_fire_target(loc14, player1.number);
    assert_equal(1, end.x == 4 || end.x == 6);
    assert_equal(1, end.y == 4 || end.y == 6);
    get_cell_coord(4, 6)->annihilated = 1;
    get_cell_coord(6, 4)->annihilated = 1;
    test_torpedo(6, 6, player1.number, 6, 3);
}

static void test_damaged_ship_at()
{
    ship_t ship;
    ship_init(&ship, CROISAL);
    for (int i = 0; i < ship_length(&ship); i++)
        assert_equal(0, ship.states[i]);
    ship.loc = (location_t){0, 0};
    damaged_ship_at(&ship, (location_t){0, 0});
    assert_equal(1, ship.states[0]);
    damaged_ship_at(&ship, (location_t){0, 1});
    assert_equal(1, ship.states[1]);

    damaged_ship_at(NULL, (location_t){0, 0});
    damaged_ship_at(&ship, create_location());
}

static void test_get_random_start_point()
{
    ship_t ship;
    ship_init(&ship, CROISAL);
    for (int i = 0; i < ship_length(&ship); i++)
        assert_equal(0, ship.states[i]);
    ship.loc = (location_t){0, 0};
    int res = get_random_start_point(&ship);
    assert_equal(1, res >= 0 && res < ship_length(&ship));
    ship.states[2] = 1;
    res = get_random_start_point(&ship);
    assert_equal(1, res != 2);
    for (int i = 0; i < ship_length(&ship) - 1; i++)
        ship.states[i] = 1;
    res = get_random_start_point(&ship);
    assert_equal(1, res == ship_length(&ship) - 1);
}

void ship_test()
{
    printf("\033[49;93m* Ship tests\033[0m\n");
    printf("\t+ ship_init\n");
    test_ship_init();
    printf("\t+ ship_length\n");
    test_ship_length();
    printf("\t+ ship_still_alive\n");
    test_ship_still_alive();
    printf("\t+ get_fire_target\n");
    test_get_fire_target();
    printf("\t+ damaged_ship_at\n");
    test_damaged_ship_at();
    printf("\t+ get_random_start_point\n");
    test_get_random_start_point();
}

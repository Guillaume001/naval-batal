#include <grid.h>
#include <location.h>
#include <ship.h>
#include <player.h>
#include <grid_test.h>
#include <assert_test.h>

static void test_initialize_grid()
{
    initialize_grid();
    for (int i = 0; i < GRID_LENGTH; i++)
        for (int j = 0; j < GRID_LENGTH; j++)
        {
            assert_equal(0, get_cell_loc((location_t){i, j})->annihilated);
            for (int o = 0; o < NBR_PLAYER; o++)
                assert_equal(1, get_cell_loc((location_t){i, j})->ships[o] == NULL);
        }
}

static void test_ship_insert()
{
    initialize_grid();
    initialize_players();

    player_t player1 = *get_player(0);
    ship_t ship1 = player1.ships[player1.ships_number - 1];
    location_t loc1 = {.x = 5, .y = 4};
    assert_equal(1, ship_insert(&player1, &ship1, loc1));
    assert_equal(5, ship1.loc.x);
    assert_equal(4, ship1.loc.y);

    for (int i = 0; i < ship_length(&ship1); i++)
        assert_equal(1, get_cell_loc((location_t){loc1.x, loc1.y + i})->ships[player1.number] == &ship1);
    assert_equal(1, get_cell_loc((location_t){loc1.x, loc1.y + ship_length(&ship1)})->ships[player1.number] == NULL);

    assert_equal(0, ship_insert(&player1, &ship1, loc1));

    ship_t ship2 = player1.ships[0];
    location_t loc2 = {.x = 5, .y = 5};
    assert_equal(0, ship_insert(&player1, &ship2, loc2));
    assert_equal(-1, ship2.loc.x);
    assert_equal(-1, ship2.loc.y);

    ship_t ship3 = player1.ships[3];
    location_t loc3 = {.x = 2, .y = 8};
    assert_equal(0, ship_insert(&player1, &ship3, loc3));

    player_t player2 = *get_player(1);
    ship_t ship11 = player2.ships[player2.ships_number - 1];
    location_t loc11 = {.x = 5, .y = 5};
    assert_equal(1, ship_insert(&player2, &ship11, loc11));
    for (int i = 0; i < ship_length(&ship11); i++)
        assert_equal(1, get_cell_loc((location_t){loc11.x, loc11.y + i})->ships[player2.number] == &ship11);
    assert_equal(0, ship_insert(&player2, &ship11, loc2));

    ship_t ship12 = player2.ships[0];
    location_t loc12 = {.x = -1, .y = -1};
    assert_equal(0, ship_insert(&player2, &ship12, loc12));
    location_t loc13 = {.x = -1, .y = 5};
    assert_equal(0, ship_insert(&player2, &ship12, loc13));
    location_t loc14 = {.x = 12, .y = -4};
    assert_equal(0, ship_insert(&player2, &ship12, loc14));
    location_t loc15 = {.x = -2, .y = 10};
    assert_equal(0, ship_insert(&player2, &ship12, loc15));
    location_t loc16 = {.x = 12, .y = 10};
    assert_equal(0, ship_insert(&player2, &ship12, loc16));
}

static void test_get_cell_loc()
{
    location_t loc = create_location();
    assert_equal(1, get_cell_loc(loc) == NULL);
    assert_equal(1, get_cell_loc((location_t){0, 1}) != NULL);
}

static void test_annihilate_location()
{
    initialize_grid();
    initialize_players();
    annihilate_location(create_location());
    player_t player1 = *get_player(0);
    ship_t ship1 = player1.ships[player1.ships_number - 1];
    location_t loc1 = {.x = 5, .y = 5};
    assert_equal(1, ship_insert(&player1, &ship1, loc1));
    annihilate_location(loc1);
    assert_equal(1, get_cell_loc(loc1)->annihilated);
    assert_equal(1, ship1.states[0]);
    loc1.y += 2;
    annihilate_location(loc1);
    assert_equal(1, get_cell_loc(loc1)->annihilated);
    assert_equal(1, ship1.states[2]);
}

void grid_test()
{
    printf("\033[49;93m* Grid tests\033[0m\n");
    printf("\t+ initialize_grid\n");
    test_initialize_grid();
    printf("\t+ ship_insert\n");
    test_ship_insert();
    printf("\t+ get_cell_loc\n");
    test_get_cell_loc();
    printf("\t+ annihilate_location\n");
    test_annihilate_location();
}
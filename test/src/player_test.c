#include <grid.h>
#include <player.h>
#include <ship_test.h>
#include <assert_test.h>

static void test_get_player()
{
    initialize_grid();
    initialize_players();
    
    assert_equal(1, get_player(-1) == NULL);
    assert_equal(1, get_player(NBR_PLAYER) == NULL);
    assert_equal(1, get_player(NBR_PLAYER + 1) == NULL);

    assert_equal(1, get_player(0)->number == 0);
    assert_equal(1, get_player(1)->number == 1);
}

static void test_one_ship_still_alive()
{
    initialize_grid();
    initialize_players();
    player_t player = *get_player(0);
    assert_equal(1, one_ship_still_alive(&player));
    for (int i = 1; i < player.ships_number - 1; i++)
    {
        sunk_ship(&player.ships[i]);
        assert_equal(1, one_ship_still_alive(&player));
    }
    sunk_ship(&player.ships[0]);
    assert_equal(1, one_ship_still_alive(&player));
    sunk_ship(&player.ships[player.ships_number - 1]);
    assert_equal(0, one_ship_still_alive(&player));
}

static void test_insert_ship()
{
    initialize_grid();
    for (int i = 0; i < NBR_PLAYER; i++)
        for (int j = 0; j < get_player(i)->ships_number; j++)
            assert_equal(0, is_inside_grid(get_player(i)->ships[j].loc));

    initialize_players();
    insert_ships();
    for (int i = 0; i < NBR_PLAYER; i++)
        for (int j = 0; j < get_player(i)->ships_number; j++)
            assert_equal(1, is_inside_grid(get_player(i)->ships[j].loc));
}

static void test_player_can_play()
{
    initialize_grid();
    initialize_players();

    player_t *player1 = get_player(0);
    ship_t *ship11 = &player1->ships[CROISAL];
    location_t loc11 = {.x = 0, .y = 0};
    assert_equal(1, ship_insert(player1, ship11, loc11));
    assert_equal(0, player_can_play(player1));

    player_t *player2 = get_player(1);
    ship_t *ship21 = &player2->ships[PORTAVIAL];
    location_t loc21 = {.x = 6, .y = 0};
    assert_equal(1, ship_insert(player2, ship21, loc21));
    assert_equal(0, player_can_play(player1));
    assert_equal(0, player_can_play(player2));

    ship_t *ship22 = &player2->ships[SUBAQUAL];
    location_t loc22 = {.x = 0, .y = 8};
    assert_equal(1, ship_insert(player2, ship22, loc22));
    assert_equal(1, player_can_play(player1));
    ship22->states[0] = 1;
    assert_equal(0, player_can_play(player1));

    ship_t *ship23 = &player2->ships[CROISAL];
    location_t loc23 = {.x = 5, .y = 0};
    assert_equal(1, ship_insert(player2, ship23, loc23));
    assert_equal(1, player_can_play(player1));
    for (int i = 0; i < ship_length(ship11); i++)
        ship11->states[i] = 1;
    assert_equal(0, player_can_play(player1));
}

static void test_get_action()
{
    initialize_grid();
    initialize_players();
    insert_ships();
    action_t action = get_action(get_player(0));
    assert_equal(1, is_inside_grid(action.start_point));
    assert_equal(1, is_inside_grid(action.target_point));
}

void player_test()
{
    printf("\033[49;93m* Player tests\033[0m\n");
    printf("\t+ get_player\n");
    test_get_player();
    printf("\t+ one_ship_still_alive\n");
    test_one_ship_still_alive();
    printf("\t+ insert_ship\n");
    test_insert_ship();
    printf("\t+ player_can_play\n");
    test_player_can_play();
    printf("\t+ get_action\n");
    test_get_action();
}
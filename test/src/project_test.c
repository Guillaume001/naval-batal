#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <ship_test.h>
#include <player_test.h>
#include <location_test.h>
#include <grid_test.h>

/**
 * Main function of the tests
 */
int main()
{
    printf("\n\033[49;94mTest launch\033[0m\n");

    ship_test();
    player_test();
    location_test();
    grid_test();

    printf("\033[1;32mTests Passed\033[0m \n\n");
    return EXIT_SUCCESS;
}

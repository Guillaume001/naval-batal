#include <grid.h>
#include <location.h>
#include <assert_test.h>

static void test_get_random_position()
{
    for (int i = 0; i < 200; i++)
    {
        location_t loc = get_random_position();
        assert_equal(1, (loc.x < GRID_LENGTH && loc.x >= 0));
        assert_equal(1, (loc.y < GRID_LENGTH && loc.y >= 0));
    }
}

void location_test()
{
    printf("\033[49;93m* Location tests\033[0m\n");
    printf("\t+ get_random_position\n");
    test_get_random_position();
}